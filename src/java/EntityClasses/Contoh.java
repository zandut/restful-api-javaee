/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
@Entity
@Table(name = "contoh")
@XmlRootElement
@NamedQueries(
{
    @NamedQuery(name = "Contoh.findAll", query = "SELECT c FROM Contoh c"),
    @NamedQuery(name = "Contoh.findByNim", query = "SELECT c FROM Contoh c WHERE c.nim = :nim")
})
public class Contoh implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NIM")
    private String nim;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "NAMA")
    private String nama;

    public Contoh()
    {
    }

    public Contoh(String nim)
    {
        this.nim = nim;
    }

    public Contoh(String nim, String nama)
    {
        this.nim = nim;
        this.nama = nama;
    }

    public String getNim()
    {
        return nim;
    }

    public void setNim(String nim)
    {
        this.nim = nim;
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (nim != null ? nim.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contoh))
        {
            return false;
        }
        Contoh other = (Contoh) object;
        if ((this.nim == null && other.nim != null) || (this.nim != null && !this.nim.equals(other.nim)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "EntityClasses.Contoh[ nim=" + nim + " ]";
    }
    
}
